﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Izersoft.WebUI.Models;
using Izersoft.Service.Category;
using Izersoft.Core.Domain;

namespace Izersoft.Controllers
{
    public class HomeController : Controller
    {
        private ICategoryService categoryService;

        public HomeController(ICategoryService _categoryService)
        {
            categoryService = _categoryService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Services()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult ArgeWorks()
        {
            return View();

        }

        public IActionResult Arge()
        {
            return View();

        }

        public IActionResult ArgeTeam()
        {
            return View();

        }

        public IActionResult Ecommerce()
        {
            return View();
        }

        public IActionResult MobileApp()
        {
            return View();
        }

        public IActionResult DomainTescil()
        {
            return View();
        }

        public IActionResult CorporateIdentityWork()
        {
            return View();
        }

        public IActionResult CorporateWebSite()
        {
            return View();
        }

        public IActionResult SpecialSoftwareSolution()
        {
            return View();
        }

        public IActionResult InternetAdvertisement()
        {
            return View();
        }

        public IActionResult SocialMediaManagement()
        {
            return View();
        }

        public IActionResult AutomationSystem()
        {
            return View();
        }

        public IActionResult InternetOfThings()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult MissionVision()
        {
            return View();
        }


        public IActionResult OurTeam()
        {
            return View();
        }


        public IActionResult SocialMedia()
        {
            return View();
        }


        public IActionResult HumanResources()
        {
            return View();
        }

        public IActionResult OurWorks()
        {
            return View();
        }

        public IActionResult WorkedWith()
        {
            return View();
        }

        public IActionResult Products()
        {
            return View();
        }

        public IActionResult ProductDetails()
        {
            return View();
        }

        public IActionResult Product(int? id)
        {
            id = 1;
            Category_ currentPage = categoryService.GetCategoryById(id.Value);
            ViewBag.HtmlContext = currentPage.CategoryContent;
            return View();
        }

        public IActionResult Basket()
        {
            return View();
        }

        public IActionResult Chechkout()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
