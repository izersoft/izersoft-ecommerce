﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Izersoft.Service.Product;
using Microsoft.AspNetCore.Mvc;


namespace Izersoft.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ProductController : Controller
    {
        private IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View(_productService.GetAllProduct());
                
        }
    }
}
