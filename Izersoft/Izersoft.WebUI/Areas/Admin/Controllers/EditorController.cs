﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Izersoft.Core.Domain;
using Izersoft.Service.Category;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Izersoft.Controllers
{
    [Area("Admin")]
    public class EditorController : Controller
    {
        private ICategoryService _categoryService;
        public EditorController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        public IActionResult Index(int? id)
        {
            if (id > 0)
            {
                ViewData["HtmlData"] = "";
            }
            else
            {
                ViewData["HtmlData"] = "<p>This is the initial editor content.</p>";
            }
            return View();
        }

        public IActionResult Index1(int? id)
        {
            return View();
        }
        public IActionResult SavePage(string htmldata)
        {
            //int id = 1;
            //Category_ category_ = _categoryService.GetCategoryById(id);
            //category_.CategoryContent = htmldata;
            //_categoryService.UpdateCategory(category_);
            ViewBag.HtmlPage = htmldata;
            return View("/Admin/Category");
        }
    }
}
