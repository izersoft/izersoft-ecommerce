﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Izersoft.Core.Domain;
using Izersoft.Service.Category;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Izersoft.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CategoryController : Controller
    {
        private ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        public IActionResult Index()
        {
            return View(_categoryService.GetAllCategory());
        }

        public IActionResult CategoryDetail(int? id)
        {
            var model = _categoryService.GetCategoryById(id.Value);
            return View(model);
        }
        public IActionResult Delete()
        {
            return View();
        }
        public IActionResult Update(int? id)
        {
            var model = _categoryService.GetCategoryById(id.Value);
            return View(model);
        }

        [HttpPost]
        public IActionResult Update(Category_ category)
        {
            _categoryService.UpdateCategory(category);
            return View(category);
        }

        public IActionResult Insert()
        {
            return View();
        }
    }
}
