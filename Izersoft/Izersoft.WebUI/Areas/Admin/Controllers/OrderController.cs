﻿using System.Linq;
using Izersoft.Service.Order;
using Microsoft.AspNetCore.Mvc;

namespace Izersoft.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class OrderController : Controller
    {
        private IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }
        public IActionResult Index()
        {
            return View(_orderService.GetAllOrder());
        }
    }
}