﻿using Izersoft.Service.Category;
using Microsoft.AspNetCore.Mvc;

namespace Izersoft.ViewComponents
{
    public class CategoryListViewComponent:ViewComponent
    {
        private ICategoryService _categoryService;
        public CategoryListViewComponent(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public IViewComponentResult Invoke()
        {
            return View(_categoryService.GetAllCategory());
        }
    }
}