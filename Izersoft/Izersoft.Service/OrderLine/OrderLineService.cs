﻿using System;
using Izersoft.Core.Data;
using Izersoft.Core.Domain;

namespace Izersoft.Service.OrderLine
{
    public class OrderLineService:IOrderLineService
    {
        private readonly IEntityRepository<OrderLine_> _orderLineRepository;
        public OrderLineService(IEntityRepository<OrderLine_> orderLineRepository)
        {
            _orderLineRepository = orderLineRepository;
        }
        public void InsertOrderLine(OrderLine_ orderLine)
        {

            if (orderLine == null)
                throw new ArgumentNullException(nameof(orderLine));

            //insert
            _orderLineRepository.Insert(orderLine);

        }

        public void DeleteOrderLine(OrderLine_ orderLine)
        {
            if (orderLine == null)
                throw new ArgumentNullException(nameof(orderLine));

            _orderLineRepository.Delete(orderLine);
        }

        public OrderLine_ GetOrderLineById(int orderLineId)
        {
            if (orderLineId == 0)
                return null;
            return _orderLineRepository.GetById(orderLineId);
        }

        public void UpdateOrderLine(OrderLine_ orderLine)
        {
            if (orderLine == null)
                throw new ArgumentNullException(nameof(orderLine));
            //update
            _orderLineRepository.Update(orderLine);
        }
    }
}