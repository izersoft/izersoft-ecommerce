﻿using Izersoft.Core.Domain;

namespace Izersoft.Service.OrderLine
{
    public interface IOrderLineService
    {
        void InsertOrderLine(OrderLine_ orderLine);
        void DeleteOrderLine(OrderLine_ orderLine);
        OrderLine_ GetOrderLineById(int orderLineId);
        void UpdateOrderLine(OrderLine_ orderLine);
    }
}