﻿using System;
using Izersoft.Core.Domain;

namespace Izersoft.Service.Address
{
    public interface IAddressService
    {
        void InsertAddress(Address_ address);
        void DeleteAddress(Address_ address);
        Address_ GetAddressById(int addressId);
        void UpdateAddress(Address_ address);
    }
}