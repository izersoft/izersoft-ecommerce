﻿using System;
using Izersoft.Core.Data;
using Izersoft.Core.Domain;

namespace Izersoft.Service.Address
{
    public class AddressService:IAddressService
    {
        private readonly IEntityRepository<Address_> _addressRepository;
        public AddressService(IEntityRepository<Address_> addressRepository)
        {
            _addressRepository = addressRepository;
        }
        public void InsertAddress(Address_ address)
        {

            if (address == null)
                throw new ArgumentNullException(nameof(address));

            //insert
            _addressRepository.Insert(address);

        }

        public void DeleteAddress(Address_ address)
        {
            if (address == null)
                throw new ArgumentNullException(nameof(address));
            _addressRepository.Delete(address);
        }

        public Address_ GetAddressById(int addressId)
        {
            if (addressId == 0)
                return null;
            return _addressRepository.GetById(addressId);
        }

        public void UpdateAddress(Address_ address)
        {
            if (address == null)
                throw new ArgumentNullException(nameof(address));
            //update
            _addressRepository.Update(address);
        }

        
    }
}