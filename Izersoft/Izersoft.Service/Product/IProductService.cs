﻿
using System.Collections.Generic;
using Izersoft.Core.Domain;

namespace Izersoft.Service.Product
{
    public interface IProductService
    {
        IList<Product_> GetAllProduct();
        void InsertProduct(Product_ product);
        void DeleteProduct(Product_ product);
        Product_ GetProductById(int productId);
        void UpdateProduct(Product_ product);
    }
}