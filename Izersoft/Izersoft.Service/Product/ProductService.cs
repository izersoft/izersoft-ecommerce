﻿using System;
using System.Collections.Generic;
using System.Linq;
using Izersoft.Core.Data;
using Izersoft.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Izersoft.Service.Product
{
    public class ProductService:IProductService
    {
        private readonly IEntityRepository<Product_> _productRepository;
        public ProductService(IEntityRepository<Product_> productRepository)
        {
            _productRepository = productRepository;
        }

        public IList<Product_> GetAllProduct()
        {
            return _productRepository.Table.Include("Category").ToList();
        }

        public void InsertProduct(Product_ product) {

            if (product == null)
                throw new ArgumentNullException(nameof(product));

            //insert
            _productRepository.Insert(product);

        }

        public void DeleteProduct(Product_ product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product));

           _productRepository.Delete(product);
        }

        public Product_ GetProductById(int productId)
        {
            if (productId == 0)
                return null;
            return _productRepository.GetById(productId);
        }

        public void UpdateProduct(Product_ product) 
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product));
            //update
            _productRepository.Update(product);
        }
    }
}