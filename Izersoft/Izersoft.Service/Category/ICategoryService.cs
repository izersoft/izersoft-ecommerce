﻿using System.Collections.Generic;
using Izersoft.Core.Domain;

namespace Izersoft.Service.Category
{
    public interface ICategoryService
    {
        IList<Category_> GetAllCategory();
        void InsertCategory(Category_ category);
        void DeleteCategory(Category_ category);
        Category_ GetCategoryById(int categoryId);
        void UpdateCategory(Category_ category);
    }
}