﻿using System;
using System.Collections.Generic;
using System.Linq;
using Izersoft.Core.Data;
using Izersoft.Core.Domain;

namespace Izersoft.Service.Category
{
    public class CategoryService:ICategoryService
    {
        private readonly IEntityRepository<Category_> _categoryRepository;
        public CategoryService(IEntityRepository<Category_> categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }


        public IList<Category_> GetAllCategory()
        {
            return _categoryRepository.Table.ToList();
        }

        public void InsertCategory(Category_ category)
        {
            if (category == null)
                throw new ArgumentNullException(nameof(category));

            //insert
            _categoryRepository.Insert(category);
        }

        public void DeleteCategory(Category_ category)
        {
            if (category == null)
                throw new ArgumentNullException(nameof(category));

            _categoryRepository.Delete(category);
        }

        public Category_ GetCategoryById(int categoryId)
        {
            if (categoryId == 0)
                return null;
            return _categoryRepository.GetById(categoryId);
        }

        public void UpdateCategory(Category_ category)
        {
            if (category == null)
                throw new ArgumentNullException(nameof(category));
            //update
            _categoryRepository.Update(category);
        }
    }
}