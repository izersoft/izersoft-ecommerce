﻿using Izersoft.Core.Domain;

namespace Izersoft.Service.Customer
{
    public interface ICustomerService
    {
        void InsertCustomer(Customer_ customer);
        void DeleteCustomer(Customer_ customer);
        Customer_ GetCustomerById(int customerId);
        void UpdateCustomer(Customer_ customer);
    }
}