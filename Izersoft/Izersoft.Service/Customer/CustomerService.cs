﻿using System;
using Izersoft.Core.Data;
using Izersoft.Core.Domain;

namespace Izersoft.Service.Customer
{
    public class CustomerService:ICustomerService
    {
        private readonly IEntityRepository<Customer_> _customerRepository;

        public CustomerService(IEntityRepository<Customer_> customerRepository)
        {
            _customerRepository = customerRepository;
        }
        public void InsertCustomer(Customer_ customer)
        {

            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            //insert
            _customerRepository.Insert(customer);

        }

        public void DeleteCustomer(Customer_ customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            _customerRepository.Delete(customer);
        }

        public Customer_ GetCustomerById(int customerId)
        {
            if (customerId == 0)
                return null;
            return _customerRepository.GetById(customerId);
        }

        public void UpdateCustomer(Customer_ customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));
            //update
            _customerRepository.Update(customer);
        }
    }
}