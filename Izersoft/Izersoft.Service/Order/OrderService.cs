﻿using System;
using System.Collections.Generic;
using System.Linq;
using Izersoft.Core.Data;
using Izersoft.Core.Domain;
using Microsoft.AspNetCore.Mvc;

namespace Izersoft.Service.Order
{
    public class OrderService:IOrderService
    {

        private readonly IEntityRepository<Order_> _orderRepository;
        public OrderService(IEntityRepository<Order_> orderRepository)
        {
            _orderRepository = orderRepository;
        }
        public IList<Order_> GetAllOrder()
        {
            return _orderRepository.Table.ToList();
        }

        public void InsertOrder(Order_ order)
        {

            if (order == null)
                throw new ArgumentNullException(nameof(order));

            //insert
            _orderRepository.Insert(order);

        }

        public void DeleteOrder(Order_ order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            _orderRepository.Delete(order);
        }

        public Order_ GetOrderById(int orderId)
        {
            if (orderId == 0)
                return null;
            return _orderRepository.GetById(orderId);
        }

        public void UpdateOrder(Order_ order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));
            //update
            _orderRepository.Update(order);
        }
    }
}