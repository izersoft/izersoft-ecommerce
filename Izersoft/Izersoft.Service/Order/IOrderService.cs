﻿using System.Collections.Generic;
using Izersoft.Core.Domain;

namespace Izersoft.Service.Order
{
    public interface IOrderService
    {
        IList<Order_> GetAllOrder();
        void InsertOrder(Order_ order);
        void DeleteOrder(Order_ order);
        Order_ GetOrderById(int orderId);
        void UpdateOrder(Order_ order);
    }
}