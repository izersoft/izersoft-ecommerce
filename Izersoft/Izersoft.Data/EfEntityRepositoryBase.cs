﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;
using Izersoft.Core.Data;
using Izersoft.Core.Domain;

namespace Izersoft.Data
{
    public class EfEntityRepositoryBase<T> : IEntityRepository<T> where T : class, IEntity, new()
    {
        protected readonly DbContext _context;
        private DbSet<T> _entities;
        public EfEntityRepositoryBase(DbContext context)
        {
            _context = context;
        }
        public T GetById(int id)
        {
            return Entities.Find(id);
        }
        public void Insert(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException(nameof(entity));

                Entities.Add(entity);

                _context.SaveChanges();
            }
            catch (Exception dbEx)
            {
                throw new Exception("Error :",dbEx);
            }
        }
        public void Update(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException(nameof(entity));
                Entities.Update(entity);
                _context.SaveChanges();
            }
            catch (Exception dbEx)
            {
                //ensure that the detailed error text is saved in the Log
                throw new Exception("Error:",dbEx);
            }
        }
        public void Delete(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException(nameof(entity));

                Entities.Remove(entity);

                _context.SaveChanges();
            }
            catch (Exception dbEx)
            {
                //ensure that the detailed error text is saved in the Log
                throw new Exception("Error : ", dbEx);
            }
        }
        public virtual IQueryable<T> Table => Entities;
        protected virtual DbSet<T> Entities => _entities ?? (_entities = _context.Set<T>());
    }
}