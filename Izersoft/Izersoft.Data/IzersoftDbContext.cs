﻿using Izersoft.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Izersoft.Data
{
    public class IzersoftDbContext:DbContext
    {
        public IzersoftDbContext(DbContextOptions<IzersoftDbContext> options):base(options)
        {
            
        }
        private DbSet<Address_> Address { get; set; }
        private DbSet<Category_> Categories { get; set; }
        private DbSet<Product_> Products { get; set; }
        private DbSet<Customer_> Customers { get; set; }
        private DbSet<Image_> Images { get; set; }
        private DbSet<Order_> Orders { get; set; }
        private DbSet<OrderLine_> OrderLines { get; set; }

    }
}