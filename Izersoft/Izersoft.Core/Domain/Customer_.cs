﻿using System.ComponentModel.DataAnnotations;

namespace Izersoft.Core.Domain
{
    public class Customer_:IEntity
    {
        [Key]
        public int CustomerId { get; set; }
        public int AdresId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Company { get; set; }
        public string Note { get; set; }

        public virtual Address_ Address { get; set; }
    }
}