﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Mime;

namespace Izersoft.Core.Domain
{
    public class Product_:IEntity
    {
        public Product_()
        {
            CreateDate = DateTime.Now;
        }
        [Key]
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
        public string Photo { get; set; }
        public bool IsFeatured { get; set; }
        public bool IsPublished { get; set; }
        public bool IsDelete { get; set; }
        public DateTime? CreateDate { get; set; }

        public Category_ Category { get; set; }
        public List<Image_> Images { get; set; }

    }
}