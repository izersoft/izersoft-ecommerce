﻿using System.ComponentModel.DataAnnotations;

namespace Izersoft.Core.Domain
{
    public class Image_:IEntity
    {
        [Key]
        public int ImageId { get; set; }
        public int ProductId { get; set; }
        public string ImageName { get; set; }

        public Product_ Product { get; set; }
    }
}