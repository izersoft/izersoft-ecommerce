﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Izersoft.Core.Domain
{
    public class Category_:IEntity
    {
        [Key]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryContent { get; set; }
        public bool IsDelete { get; set; }

        public IList<Product_> Product { get; set; }
    }
}
