﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Izersoft.Core.Domain
{
    public class OrderNote_:IEntity
    {
        [Key]
        public int OrderNoteId { get; set; }
        public int OrderId { get; set; }
        public string Note { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public virtual Order_ Order { get; set; }
    }
}