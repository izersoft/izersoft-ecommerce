﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Izersoft.Core.Domain
{
    public class Order_:IEntity
    {
        public Order_()
        {
            OrderLines = new List<OrderLine_>();
        }
        [Key]
        public int OrderId { get; set; }
        public Guid OrderGuid { get; set; }

        public int ApplicationUserId { get; set; }
        public int BillingAddressId { get; set; }
        public int? ShippingAddressId { get; set; }
        public int OrderStatusId { get; set; }
        public int PaymentStatusId { get; set; }
        public string PaymentMethodSystemName { get; set; }
        public string VatNumber { get; set; }
        public decimal OrderSubtotalInclTax { get; set; }
        public decimal OrderSubtotalExclTax { get; set; }
        public decimal OrderSubTotalDiscountInclTax { get; set; }
        public decimal OrderSubTotalDiscountExclTax { get; set; }
        public decimal OrderDiscount { get; set; }
        public decimal OrderTotal { get; set; }
        public decimal RefundedAmount { get; set; }
        public decimal RefundedAmountDesc { get; set; }
        public string AuthorizationTransactionId { get; set; }
        public string AuthorizationTransactionCode { get; set; }
        public string AuthorizationTransactionResult { get; set; }
        public string CaptureTransactionId { get; set; }
        public string CaptureTransactionResult { get; set; }
        public string SubscriptionTransactionId { get; set; }
        public DateTime? PaidDateUtc { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedOnUtc { get; set; }

        public virtual Customer_ Cumtomer { get; set; }
        public virtual Address_ BillingAddress { get; set; }
        public virtual Address_ ShippingAddress { get; set; }
        public virtual IList<OrderLine_> OrderLines { get; set; }
    }
}