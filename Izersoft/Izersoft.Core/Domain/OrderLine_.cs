﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Izersoft.Core.Domain
{
    public class OrderLine_:IEntity
    {
        [Key]
        public int OrderLineId { get; set; }
        public Guid OrderItemGuid { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPriceInclTax { get; set; }
        public decimal UnitPriceExclTax { get; set; }
        public decimal PriceInclTax { get; set; }
        public decimal PriceExclTax { get; set; }
        public decimal DiscountAmountInclTax { get; set; }
        public decimal DiscountAmountExclTax { get; set; }
        public virtual Order_ Order { get; set; }
        public virtual Product_ Product { get; set; }
    }
}