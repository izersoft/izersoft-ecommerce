﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Izersoft.Core.Domain
{
    public class Address_:IEntity
    {
        [Key]
        public int AddressId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }
        public int? CountryId { get; set; }
        public string City { get; set; }
        public string ZipPostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreatedOnUtc { get; set; }

        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string FullAddress { get; set; }

        public IList<Customer_> Customers { get; set; }
    }
}