﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Izersoft.Core.Domain;

namespace Izersoft.Core.Data
{
    public interface IEntityRepository<T> where T:class,IEntity,new()
    {
        T GetById(int id);
        void Insert(T entity);
        void Delete(T entity);
        void Update(T entity);
        IQueryable<T> Table { get; }
    }
}