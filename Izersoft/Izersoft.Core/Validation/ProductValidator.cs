﻿using FluentValidation;
using Izersoft.Core.Domain;

namespace Izersoft.Core.Validation
{
    public class ProductValidator:AbstractValidator<Product_>
    {
        public ProductValidator()
        {
            RuleFor(x => x.ProductId).NotNull();
            RuleFor(x => x.IsFeatured).NotNull();
            RuleFor(x => x.IsPublished).NotNull();
            RuleFor(x => x.Name).NotNull();
            RuleFor(x => x.Price).NotNull();
            RuleFor(x => x.Stock).NotNull();
        }
    }
}