﻿using System;
using System.Linq;
using Izersoft.Core.CrossCuttingConcerns.Validation;

namespace Izersoft.Core.Aspect.ValidationAspects
{
    public class FluentValidationAspect
    {
        Type _validatorType;
        public FluentValidationAspect(Type validatorType)
        {
            _validatorType = validatorType;
        }
    }
}